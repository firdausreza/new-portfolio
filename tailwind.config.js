/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  theme: {
    extend: {
      colors: {
        'custom-brown': '#663300',
        'custom-orange': '#CC6600',
        'yellow-orange': '#333300',
        'yellow-green': '#CC9900'
      },
      width: {
        200: '200px',
        300: '300px',
        400: '400px',
        500: '500px'
      },
      height: {
        200: '200px',
        300: '300px',
        400: '400px',
        500: '500px'
      },
      screens: {
        'vsm': { min: '425px' },
        'six-inch': { min: '510px' },
      }
    }
  },
  plugins: [],
}

